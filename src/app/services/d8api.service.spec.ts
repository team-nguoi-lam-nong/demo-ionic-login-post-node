import { TestBed } from '@angular/core/testing';

import { D8apiService } from './d8api.service';

describe('D8apiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: D8apiService = TestBed.get(D8apiService);
    expect(service).toBeTruthy();
  });
});
