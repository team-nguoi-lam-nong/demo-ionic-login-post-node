import { Component } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  films;
  token: string ='';
  cookie: string ='';
  
  domain_name = 'dev.nguoilamnong.vn';
  
  //token_url = 'http://dabaco.quanlytruyxuat.vn/services/session/token';
  //login_url = 'http://quanlyrau.chuyengiaso.com/api/user/login.json';
  //post_url  = 'http://quanlyrau.chuyengiaso.com/api/node.json';
    
  token_url = 'https://' + this.domain_name + '/session/token';
  
  //token_url = 'https://' + this.domain_name + '/services/session/token';
  login_url_services = 'https://' + this.domain_name + '/api/user/login';
  login_url_rest = 'https://' + this.domain_name + '/user/login';
  logout_url_services = 'https://' + this.domain_name + '/api/user/logout';
  logout_url_rest = 'https://' + this.domain_name + 'user/logout';
  post_url  = 'https://' + this.domain_name + '/entity/node';
  post_url_service  = 'https://' + this.domain_name + '/node';
  post_post_url  = 'https://' + this.domain_name + '/entity/post';
  get_post_url = 'https://' + this.domain_name + '/post/2';
  
  format = '?_format=json'; 

  constructor(public httpClient: HttpClient) {}
  
  
  getToken() {
    
    this.httpClient.get(this.token_url  + this.format, { 
      withCredentials: true, 
      responseType: 'text' }
    ).subscribe(data => {
    
      this.token = data;    
      console.log('Token got: ', this.token);      
    });
    
  } // getToken()
  
  doLogin() {
  
    this.httpClient.post(
      this.login_url_rest + this.format,
      JSON.stringify(
        { // payload
          username: 'linh.pv',
          password: 'admin@123'
        }
      ),
      { 
        withCredentials: true,    // get / set cookie
        // header
        headers: {          
          "Content-Type": "application/json",
          "Accept": "application/json, text/plain, */*",
          "X-CSRF-Token": this.token
          //"Cookie": this.cookie
          //"Authentication": this.cookie
        }
      }
      
    ).subscribe(data => {
    
      console.log('cookie data', data);
    
      //this.token = data.token;    
      
      // error property does not exist
      //this.cookie = data.name + "=" + data.id;
      //this.cookie = "SSESS9fd23291a7154d44d6101a0177e48c03__=lqZBWnBBGBLa6giXWgEEd8NeIqmv87ybR1iY2mw9YAQ__";
      
      //console.log('Cookie: ', this.cookie);
      
      // set cookie now to web browser
      //document.cookie = this.cookie;
    });
    
  } // doLogin()
  
  doLogout() {
    
    this.httpClient.post(
      this.logout_url_rest  + this.format,
      JSON.stringify({}),
      { // header
        withCredentials: true,    // uses cookie
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json, text/plain, */*",
          "X-CSRF-Token": this.token,
          //"Authentication": this.cookie
        }        
      }      
    ).subscribe(data => {
    
      console.log('cookie data', data);
    
    });
  }
  
  doPostNode() {
  
    this.httpClient.post(
      this.post_url  + this.format,
      JSON.stringify(
      /*
        { // payload
          title : ['Node 1'],
          type : 'page',
        } 
        */
        
        {
          "title": [
            { "value": "atest2" }
          ],
          "type": [
            { "target_id": "mxh_ionic" }
          ]

          }

      ),
      { // header
        withCredentials: true,    // uses cookie
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json, text/plain, */*",
          "X-CSRF-Token": this.token,
          "Authentication": this.cookie
        }        
      }
      
    ).subscribe(data => {
    
      console.log('Node Post response data: ', data);
      
      
    });
    
  } // doPost()
  
  doPostNodeService() {
  
    this.httpClient.post(
      this.post_url_service  + this.format,
      JSON.stringify(
      /*
        { // payload
          title : ['Node 1'],
          type : 'page',
        } 
        */
        
        {
          "title": [
            { "value": "atest2" }
          ],
          "type": [
            { "target_id": "mxh_ionic" }
          ]

          }

      ),
      { // header
        withCredentials: true,    // uses cookie
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json, text/plain, */*",
          "X-CSRF-Token": this.token,
          "Authentication": this.cookie
        }        
      }
      
    ).subscribe(data => {
    
      console.log('Node Post response data: ', data);
      
      
    });
    
  } // doPost()
  
  doPostPost() {
  
    this.httpClient.post(
      this.post_post_url + this.format,
      JSON.stringify(
        { // payload
          field_post : ['Post 1...'],
          type : 'post',
        } 
      ),
      { // header
        headers: {
          "Content-Type": "application/json",
          //"Accept": "application/json, text/plain, */*",
          "X-CSRF-Token": this.token,
          "Authentication": this.cookie
        }        
      }
      
    ).subscribe(data => {
    
      console.log('Post Post response data: ', data);
      //console.log('New nid: ', data.nid);
      //console.log('New node uri: ', data.nid);
      
    });
    
  } // doPost()
  
  doGetPost(pid) {
    
    this.httpClient.get(
      this.get_post_url + this.format,
        //{ responseType: 'text' },
      { // header
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json, text/plain",
          //"X-CSRF-Token": this.token,          
          "Authentication": this.cookie
        }        
      }
      
    ).subscribe(data => {
    
      console.log('Get Post (2) response data: ', data);
      
      
    });
    
    
  } // doPost()

}
